// Author: Vincent Jadraque
// Student ID: 11970720
// Date: 14/11/2018

// File name: sim_tf_broadcaster.cpp
/*
   Description:

for simulation - broadcasts hardcoded transform between
the base frame and the camera frame
also adds target frame at ar marker frame with roll of 180 degrees

*/

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>

uint id = 0;

// callback to get frame id from ar_pose_marker topic
void cbGetMarkerFrame(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& msg)
{
  // retrieve the frame id from the first marker in the message
  // TODO: loop through markers and search for correct marker.id

  try
  {
    id = msg->markers.at(0).id;
  }
  catch (std::out_of_range oor) {
     ROS_WARN_ONCE("%s", oor.what());
  }
}

int main(int argc, char** argv){
  ros::init(argc, argv, "robot_tf_publisher");
  ros::NodeHandle n;

  ros::Rate r(100);

  tf::TransformBroadcaster broadcaster;
  tf::TransformListener listener;
  ros::Subscriber sub = n.subscribe("ar_pose_marker", 1, cbGetMarkerFrame);
  ros::Publisher pub = n.advertise<geometry_msgs::Pose>("goal_pose", 1);

  while(n.ok()){

    broadcaster.sendTransform(
      tf::StampedTransform(
        // quat - xyzw; translation - xyz
        // quaternion changes from base orientation (z is upwards) to camera (z points to front of Sawyer)
        // translation is (0.0, 0.4, 0.3) (xyz) away from the base Quaternion(-0.5, 0.5, -0.5, 0.5)
        tf::Transform(tf::Quaternion(-0.7071, 0.0, 0.0, 0.7071), tf::Vector3(-0.03, 0.15, 0.4)),
        ros::Time::now(),"base_link", "usb_cam"));

    try
    {
      ros::spinOnce();
      std::string frame_id = "ar_marker_" + boost::lexical_cast<std::string>(id);

      broadcaster.sendTransform(
      tf::StampedTransform(
        // quat - xyzw; translation - xyz
        // 180 degree rotation to make target pose face the ar tag
        tf::Transform(tf::Quaternion(1.0, 0.0, 0.0, 0.0), tf::Vector3(0.0, 0.0, 0.0)),
        // TODO: make code more robust by automating the marker frame id - make callback subscriber
        ros::Time::now(), frame_id, "target"));
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
    }

    // Send goal_pose of pose from base_link to target
    try
    {
      tf::StampedTransform transform;
      geometry_msgs::Pose goal_pose;
      listener.waitForTransform("base", "target", ros::Time::now(),ros::Duration(0.5));
      listener.lookupTransform("base", "target", ros::Time(0), transform);

      goal_pose.position.x = transform.getOrigin().getX();
      goal_pose.position.y = transform.getOrigin().getY();
      goal_pose.position.z = transform.getOrigin().getZ();
      goal_pose.orientation.x = transform.getRotation().getX();
      goal_pose.orientation.y = transform.getRotation().getY();
      goal_pose.orientation.z = transform.getRotation().getZ();
      goal_pose.orientation.w = transform.getRotation().getW();

      pub.publish(goal_pose);
    }
    catch (tf::LookupException &ex)
    {
      ROS_WARN_ONCE("%s", ex.what());
    }
    catch (tf::ConnectivityException &ex)
    {
      ROS_WARN_ONCE("%s", ex.what());
    }
    catch (tf2::ExtrapolationException &ex)
    {
      ROS_WARN_ONCE("%s", ex.what());
    }

    r.sleep();
  }
}
